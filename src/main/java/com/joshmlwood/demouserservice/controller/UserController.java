package com.joshmlwood.demouserservice.controller;

import com.joshmlwood.demouserservice.contract.RegistrationRequest;
import com.joshmlwood.demouserservice.domain.User;
import com.joshmlwood.demouserservice.service.UserService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class UserController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    private UserService userService;

    @GetMapping
    public List<User> getUsers() {
        return Collections.emptyList();
    }

    @GetMapping("/{email}")
    public User getUser(@PathVariable("email") String email) {
        return userService.getAccountForEmail(email);
    }

    @PostMapping
    public User registerUser(@RequestBody RegistrationRequest registrationRequest) {
        return userService.registerUser(registrationRequest);
    }

    @DeleteMapping("/{email}")
    public void deleteUser(@PathVariable("email") String email) {
        userService.deleteUser(email);
    }
}
