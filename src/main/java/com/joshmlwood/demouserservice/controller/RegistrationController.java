package com.joshmlwood.demouserservice.controller;

import com.joshmlwood.demouserservice.contract.RegistrationRequest;
import com.joshmlwood.demouserservice.domain.User;
import com.joshmlwood.demouserservice.service.UserService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/oauth/register")
@AllArgsConstructor
public class RegistrationController {
    private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationController.class);

    private UserService userService;

    @PostMapping
    public User registerUser(@RequestBody RegistrationRequest registrationRequest) {
        return userService.registerUser(registrationRequest);
    }
}
