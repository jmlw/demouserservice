package com.joshmlwood.demouserservice.controller;

import com.joshmlwood.demouserservice.contract.ActiveAccount;
import com.joshmlwood.demouserservice.contract.Authenticated;
import com.joshmlwood.demouserservice.contract.ExceptionResponse;
import com.joshmlwood.demouserservice.domain.User;
import com.joshmlwood.demouserservice.domain.UserRepository;
import com.joshmlwood.demouserservice.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/user")
@AllArgsConstructor
public class UserDetailsController {
    private UserService userService;


    @GetMapping
    public User getUserDetails(@Authenticated @ActiveAccount User account) {
        return account;
    }

    @GetMapping(value = "/{email}")
    @PreAuthorize("hasRole('ROLE_USER_ADMIN')")
    public User getUserDetails(@PathVariable("email") String email) {
        return userService.getAccountForEmail(email);
    }

    @ExceptionHandler(UserRepository.UserDoesNotExistException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ExceptionResponse handleUserNotFoundException() {
        return new ExceptionResponse("User with that email does not exist");
    }
}
