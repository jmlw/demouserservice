package com.joshmlwood.demouserservice.controller;

import com.joshmlwood.demouserservice.contract.AuthenticationResult;
import com.joshmlwood.demouserservice.contract.LoginRequest;
import com.joshmlwood.demouserservice.contract.RefreshRequest;
import com.joshmlwood.demouserservice.service.LoginService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("/auth")
public class LoginController {

    private LoginService loginService;

    @PostMapping("/login")
    public AuthenticationResult authenticate(@RequestBody LoginRequest loginRequest) {
        return loginService.authenticateRequest(loginRequest);
    }

    @PostMapping("/refresh")
    public AuthenticationResult refreshToken(@RequestBody RefreshRequest refreshRequest) {
        return loginService.refreshToken(refreshRequest);
    }
}
