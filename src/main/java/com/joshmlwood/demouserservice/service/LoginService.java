package com.joshmlwood.demouserservice.service;

import com.joshmlwood.demouserservice.contract.AuthenticationResult;
import com.joshmlwood.demouserservice.contract.AuthenticationToken;
import com.joshmlwood.demouserservice.contract.LoginRequest;
import com.joshmlwood.demouserservice.contract.RefreshRequest;
import com.joshmlwood.demouserservice.domain.User;
import com.joshmlwood.demouserservice.interfaces.ManagedUserService;
import com.joshmlwood.demouserservice.interfaces.LoginChain;
import com.joshmlwood.demouserservice.util.OptionalChain;
import com.joshmlwood.demouserservice.util.SimpleProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginService {
    private ManagedUserService managedUserService;
    private UserService userService;
    private SimpleProvider<LoginChain> loginChainProvider;

    @Autowired
    public LoginService(
            ManagedUserService managedUserService,
            UserService userService,
            SimpleProvider<LoginChain> loginChainProvider) {
        this.managedUserService = managedUserService;
        this.userService = userService;
        this.loginChainProvider = loginChainProvider;
    }

    public AuthenticationResult authenticateRequest(LoginRequest request) {
        User account = userService.getAccountForEmail(request.getEmail());
        if (account.getPasswordResetRequired()) {
            return buildPasswordResetRequiredAuthenticationResult();
        }
        AuthenticationToken tokenHolder = authenticateUser(request);

        return OptionalChain.chain(
                loginChainProvider.getEntries(),
                link -> link.authenticate(account, tokenHolder, request)
        ).orElseThrow(() -> new LoginException("Failed to process login request"));
    }

    public AuthenticationResult refreshToken(RefreshRequest request) {
        User account = userService.getAccountForEmail(request.getEmail());
        if (account.getPasswordResetRequired()) {
            return buildPasswordResetRequiredAuthenticationResult();
        }

        AuthenticationToken tokenHolder = managedUserService.refreshToken(request.getRefreshToken());
        return OptionalChain.chain(
                loginChainProvider.getEntries(),
                chain -> chain.authenticate(account, tokenHolder, request)
        ).orElseThrow(() -> new TokenRefreshException("Unable to refresh authentication token"));
    }

    private AuthenticationToken authenticateUser(LoginRequest request) {
        return managedUserService.authenticateUser(request.getEmail(), request.getPassword());
    }

    private AuthenticationResult buildPasswordResetRequiredAuthenticationResult() {
        return AuthenticationResult.builder()
                .requiresPasswordReset(true)
                .build();
    }

    public static final class LoginException extends RuntimeException {
        public LoginException(String message) {
            super(message);
        }
    }

    public static final class TokenRefreshException extends RuntimeException {
        public TokenRefreshException(String message) {
            super(message);
        }
    }
}
