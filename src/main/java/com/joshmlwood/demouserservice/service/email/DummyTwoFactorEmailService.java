package com.joshmlwood.demouserservice.service.email;

import com.joshmlwood.demouserservice.interfaces.TwoFactorEmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class DummyTwoFactorEmailService implements TwoFactorEmailService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DummyTwoFactorEmailService.class);

    @Override
    public void sendEmail(String email) {
        LOGGER.info("Sending two factor email for: " + email);
    }
}
