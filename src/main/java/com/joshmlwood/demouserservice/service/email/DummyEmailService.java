package com.joshmlwood.demouserservice.service.email;

import com.joshmlwood.demouserservice.contract.SendEmailRequest;
import com.joshmlwood.demouserservice.interfaces.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class DummyEmailService implements EmailService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DummyEmailService.class);

    @Override
    public void sendEmail(SendEmailRequest sendEmailRequest) {
        LOGGER.info("Sending email from request: " + sendEmailRequest.toString());
    }
}
