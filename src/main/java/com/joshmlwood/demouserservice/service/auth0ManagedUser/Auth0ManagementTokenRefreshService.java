package com.joshmlwood.demouserservice.service.auth0ManagedUser;

import com.auth0.client.auth.AuthAPI;
import com.auth0.exception.Auth0Exception;
import com.auth0.json.auth.TokenHolder;
import com.auth0.net.AuthRequest;
import com.joshmlwood.demouserservice.contract.Auth0ManagementToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class Auth0ManagementTokenRefreshService {
    private AuthAPI authAPI;
    private String requestTokenEndpoint;
    private Auth0ManagementToken auth0ManagementToken;

    @Autowired
    public Auth0ManagementTokenRefreshService(AuthAPI authAPI,
                                              @Value("${auth0.token}") String requestTokenEndpoint,
                                              Auth0ManagementToken auth0ManagementToken) {
        this.authAPI = authAPI;
        this.requestTokenEndpoint = requestTokenEndpoint;
        this.auth0ManagementToken = auth0ManagementToken;
        refresh();
    }

    private TokenHolder renewServerAuth0Token() {
        AuthRequest authRequest = authAPI.requestToken(requestTokenEndpoint);

        try {
            return authRequest.execute();
        } catch (Auth0Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Scheduled(fixedDelay = 8L * 60L * 60L * 1000L)
    public void refresh() {
        TokenHolder tokenHolder = renewServerAuth0Token();

        auth0ManagementToken.setTokenHolder(tokenHolder);
    }
}
