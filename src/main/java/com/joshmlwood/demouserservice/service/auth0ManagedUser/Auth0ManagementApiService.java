package com.joshmlwood.demouserservice.service.auth0ManagedUser;

import com.auth0.client.auth.AuthAPI;
import com.auth0.client.mgmt.ManagementAPI;
import com.auth0.client.mgmt.filter.UserFilter;
import com.auth0.json.auth.TokenHolder;
import com.auth0.json.mgmt.users.UsersPage;
import com.auth0.net.AuthRequest;
import com.auth0.net.Request;
import com.joshmlwood.demouserservice.config.Auth0ConfigurationProperties;
import com.joshmlwood.demouserservice.contract.Auth0ManagementToken;
import com.joshmlwood.demouserservice.contract.AuthenticationToken;
import com.joshmlwood.demouserservice.domain.ManagedUser;
import com.joshmlwood.demouserservice.domain.User;
import com.joshmlwood.demouserservice.interfaces.ManagedUserService;
import com.joshmlwood.demouserservice.util.Auth0RequestUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class Auth0ManagementApiService implements ManagedUserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(Auth0ManagementApiService.class);
    private static final String AUTH_PROVIDER = "auth0";

    private AuthAPI authAPI;
    private Auth0ConfigurationProperties configurationProperties;
    private Auth0ManagementToken auth0ManagementToken;

    @Autowired
    public Auth0ManagementApiService(AuthAPI authAPI, Auth0ConfigurationProperties configurationProperties, Auth0ManagementToken auth0ManagementToken) {
        this.authAPI = authAPI;
        this.configurationProperties = configurationProperties;
        this.auth0ManagementToken = auth0ManagementToken;
    }

    private ManagementAPI getManagementApi() {
        String domain = configurationProperties.getDomain();
        TokenHolder tokenHolder = auth0ManagementToken.getTokenHolder();

        return new ManagementAPI(domain, tokenHolder.getAccessToken());
    }

    @Override
    public Optional<ManagedUser> findByProviderId(String providerId) {
        LOGGER.debug(String.format("Finding user by id: %s", providerId));
        UserFilter userFilter = new UserFilter();
        userFilter.withQuery("user_id: " + providerId);

        Request<UsersPage> usersPageRequest = getManagementApi().users().list(userFilter);
        UsersPage usersPage = Auth0RequestUtil.executeRequest(usersPageRequest);
        return usersPage.getItems()
                .stream()
                .findFirst()
                .map(it -> new ManagedUser(null, AUTH_PROVIDER, it.getId()));
    }

    @Override
    public Optional<ManagedUser> findOneByEmailIgnoringCase(String email) {
        LOGGER.debug(String.format("Finding user by email: %s", email));
        UserFilter userFilter = new UserFilter();
        // auth0 save the email in lowercase
        userFilter.withQuery("email: " + email.toLowerCase());
        Request<UsersPage> usersPageRequest = getManagementApi().users().list(userFilter);
        UsersPage usersPage = Auth0RequestUtil.executeRequest(usersPageRequest);
        return usersPage.getItems()
                .stream()
                .findFirst()
                .map(it -> new ManagedUser(null, AUTH_PROVIDER, it.getId()));
    }

    @Override
    public ManagedUser createUser(User applicationAccount, String password) {
        com.auth0.json.mgmt.users.User user = new com.auth0.json.mgmt.users.User(configurationProperties.getConnection());
        Map<String, Object> userMetadata = new HashMap<>();
        Map<String, Object> appMetadata = new HashMap<>();

        user.setEmail(applicationAccount.getEmail());
        user.setPassword(password);

        user.setEmailVerified(applicationAccount.getEmailVerified());

        user.setUserMetadata(userMetadata);
        user.setAppMetadata(appMetadata);

        Request<com.auth0.json.mgmt.users.User> userRequest = getManagementApi().users().create(user);
        com.auth0.json.mgmt.users.User auth0User = Auth0RequestUtil.executeRequest(userRequest);

        return new ManagedUser(null, AUTH_PROVIDER, auth0User.getId());
    }

    @Override
    public void deleteUser(String providerId) {
        Request deleteRequest = getManagementApi().users().delete(providerId);
        Auth0RequestUtil.executeRequest(deleteRequest);
    }

    @Override
    public void changePassword(String providerId, String password) {
        com.auth0.json.mgmt.users.User user = new com.auth0.json.mgmt.users.User(configurationProperties.getConnection());
        user.setPassword(password);

        Request<com.auth0.json.mgmt.users.User> passwordChangeRequest = getManagementApi().users().update(providerId, user);
        Auth0RequestUtil.executeRequest(passwordChangeRequest);
    }

    @Override
    public Page<ManagedUser> getAllManagedUsers(Pageable pageable) {
        UserFilter userFilter = new UserFilter();
        userFilter.withPage(pageable.getPageNumber(), pageable.getPageSize());
        userFilter.withTotals(true);
        Request<UsersPage> requestUser = getManagementApi().users().list(userFilter);
        UsersPage usersPage = Auth0RequestUtil.executeRequest(requestUser);
        List<ManagedUser> list = usersPage.getItems()
                .stream()
                .map(user -> new ManagedUser(null, AUTH_PROVIDER, user.getId()))
                .collect(Collectors.toList());

        return new PageImpl<>(list, pageable, usersPage.getTotal());
    }

    @Override
    public AuthenticationToken authenticateUser(String email, String password) {
        AuthRequest authRequest = this.authAPI.login(email, password).setScope("openid email read:current_user");
        TokenHolder tokenHolder = Auth0RequestUtil.executeRequest(authRequest);
        return new AuthenticationToken(
                tokenHolder.getAccessToken(),
                tokenHolder.getRefreshToken(),
                tokenHolder.getExpiresIn()
        );
    }

    @Override
    public AuthenticationToken refreshToken(String refreshToken) {
        AuthRequest renewRequest = this.authAPI.renewAuth(refreshToken);
        TokenHolder tokenHolder = Auth0RequestUtil.executeRequest(renewRequest);
        return new AuthenticationToken(
                tokenHolder.getAccessToken(),
                tokenHolder.getRefreshToken(),
                tokenHolder.getExpiresIn()
        );
    }

    @Override
    public void enableAccount(String providerId) {
        com.auth0.json.mgmt.users.User enableUser = new com.auth0.json.mgmt.users.User("Initial-Connection");
        enableUser.setBlocked(false);

        Request<com.auth0.json.mgmt.users.User> update = getManagementApi().users().update(providerId, enableUser);
        Auth0RequestUtil.executeRequest(update);
    }

    @Override
    public void disableAccount(String providerId) {
        com.auth0.json.mgmt.users.User disableUser = new com.auth0.json.mgmt.users.User("Initial-Connection");
        disableUser.setBlocked(true);

        Request<com.auth0.json.mgmt.users.User> update = getManagementApi().users().update(providerId, disableUser);
        Auth0RequestUtil.executeRequest(update);
    }
}
