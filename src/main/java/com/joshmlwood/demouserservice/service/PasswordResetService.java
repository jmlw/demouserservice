package com.joshmlwood.demouserservice.service;

import com.joshmlwood.demouserservice.domain.PasswordResetToken;
import com.joshmlwood.demouserservice.domain.PasswordResetTokenRepository;
import com.joshmlwood.demouserservice.domain.User;
import com.joshmlwood.demouserservice.domain.UserRepository;
import com.joshmlwood.demouserservice.interfaces.ManagedUserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
@AllArgsConstructor
public class PasswordResetService {
    private UserRepository userRepository;
    private ManagedUserService managedUserService;
    private PasswordResetTokenRepository passwordResetTokenRepository;

    public PasswordResetToken generateResetToken(String email) {
        return passwordResetTokenRepository.save(new PasswordResetToken(
                null,
                UUID.randomUUID(),
                email,
                LocalDateTime.now()
        ));
    }

    public void setNewPasswordUsingToken(String password, UUID token) {
        PasswordResetToken resetToken = passwordResetTokenRepository.findByToken(token).orElseThrow();
        User account = userRepository.findByEmail(resetToken.getEmail()).orElseThrow();

        managedUserService.changePassword(resetToken.getEmail(), password);

        account.toBuilder().passwordResetRequired(false);
        userRepository.save(account);
        passwordResetTokenRepository.delete(resetToken);
    }
}
