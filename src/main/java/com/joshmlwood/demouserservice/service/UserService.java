package com.joshmlwood.demouserservice.service;

import com.joshmlwood.demouserservice.contract.RegistrationRequest;
import com.joshmlwood.demouserservice.domain.ManagedUser;
import com.joshmlwood.demouserservice.domain.User;
import com.joshmlwood.demouserservice.domain.UserRepository;
import com.joshmlwood.demouserservice.exception.EmailAlreadyRegistered;
import com.joshmlwood.demouserservice.exception.InvalidEmailFormat;
import com.joshmlwood.demouserservice.interfaces.ManagedUserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.nio.charset.Charset;
import java.util.Collections;
import java.util.Random;
import java.util.regex.Pattern;

@Service
@AllArgsConstructor
public class UserService {
    private static final Pattern BASIC_EMAIL_PATTERN = Pattern.compile("^.+@.+\\..+$");

    private final Random random = new Random();

    private UserRepository userRepository;
    private ManagedUserService managedUserService;

    public User getAccountForEmail(String email) {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException(""));
    }

    public User registerUser(RegistrationRequest registrationRequest) {
        // TODO: 2019-06-24 Send email to verify user on creation
        validateRegistrationRequest(registrationRequest);
        User user = User.builder()
                .email(registrationRequest.getEmail())
                .emailVerified(false)
                .passwordResetRequired(registrationRequest.getPassword() == null)
                .verified(false)
                .roles(Collections.emptyList())
                .twoFactorRequired(false)
                .build();
        String assignedPassword = registrationRequest.getPassword() != null ? registrationRequest.getPassword() : generateRandomPassword();
        ManagedUser managedUser = managedUserService.createUser(user, assignedPassword);
        return userRepository.save(user.toBuilder().managedUser(managedUser).build());
    }

    private void validateRegistrationRequest(RegistrationRequest registrationRequest) {
        if (userEmailDoesNotLookValid(registrationRequest.getEmail())) {
            throw new InvalidEmailFormat();
        } else if (userRepository.existsByEmail(registrationRequest.getEmail())) {
            throw new EmailAlreadyRegistered();
        }
    }

    private boolean userEmailDoesNotLookValid(String email) {
        return !BASIC_EMAIL_PATTERN.matcher(email).matches();
    }

    public void addRole(String email, String role) {
        // TODO: 2019-06-24
    }

    public void removeRole(String email, String role) {
        // TODO: 2019-06-24
    }

    public void deleteUser(String email) {
        User user = userRepository.findByEmail(email).orElseThrow(UserNotFoundException::new);
        ManagedUser managedUser = user.getManagedUser();
        if (managedUser != null && managedUser.getProviderId() != null) {
            managedUserService.deleteUser(managedUser.getProviderId());
        }
        userRepository.delete(user);
    }

    public void deactiveUser(String email) {
        // TODO: 2019-06-24
    }

    public void activateUser(String email) {
        // TODO: 2019-06-24
    }

    private String generateRandomPassword() {
        byte[] array = new byte[10];
        random.nextBytes(array);
        return new String(array, Charset.forName("UTF-8"));
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    public static class UserNotFoundException extends RuntimeException {
    }
}
