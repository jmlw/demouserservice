package com.joshmlwood.demouserservice.interfaces;

import com.joshmlwood.demouserservice.contract.AuthenticationResult;
import com.joshmlwood.demouserservice.contract.AuthenticationToken;
import com.joshmlwood.demouserservice.contract.LoginRequest;
import com.joshmlwood.demouserservice.contract.RefreshRequest;
import com.joshmlwood.demouserservice.domain.User;

import java.util.Optional;

public interface LoginChain {
    Optional<AuthenticationResult> authenticate(User user, AuthenticationToken tokenHolder, LoginRequest request);

    Optional<AuthenticationResult> authenticate(User user, AuthenticationToken tokenHolder, RefreshRequest request);
}
