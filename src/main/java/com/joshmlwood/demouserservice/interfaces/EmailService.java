package com.joshmlwood.demouserservice.interfaces;

import com.joshmlwood.demouserservice.contract.SendEmailRequest;

public interface EmailService {
    void sendEmail(SendEmailRequest sendEmailRequest);
}
