package com.joshmlwood.demouserservice.interfaces;

import com.joshmlwood.demouserservice.contract.AuthenticationToken;
import com.joshmlwood.demouserservice.domain.ManagedUser;
import com.joshmlwood.demouserservice.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface ManagedUserService {
    Optional<ManagedUser> findByProviderId(String providerId);

    Optional<ManagedUser> findOneByEmailIgnoringCase(String email);

    ManagedUser createUser(User applicationAccount, String password);

    void deleteUser(String providerId);

    void enableAccount(String providerId);

    void disableAccount(String providerId);

    void changePassword(String providerId, String password);

    Page<ManagedUser> getAllManagedUsers(Pageable pageable);

    AuthenticationToken authenticateUser(String email, String password);

    AuthenticationToken refreshToken(String refreshToken);
}
