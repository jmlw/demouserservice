package com.joshmlwood.demouserservice.interfaces;

public interface TwoFactorEmailService {
    void sendEmail(String email);
}
