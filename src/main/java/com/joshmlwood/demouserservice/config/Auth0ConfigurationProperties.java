package com.joshmlwood.demouserservice.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("auth0.management")
@Data
public class Auth0ConfigurationProperties {

    //    private String audience;
//    private String issuer;
    private String audience;
    private String domain;
    private String clientId;
    private String clientSecret;
    private String connection;


}
