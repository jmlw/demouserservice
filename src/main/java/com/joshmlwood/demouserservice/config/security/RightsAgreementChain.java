package com.joshmlwood.demouserservice.config.security;

import com.joshmlwood.demouserservice.contract.AuthenticationResult;
import com.joshmlwood.demouserservice.contract.AuthenticationToken;
import com.joshmlwood.demouserservice.contract.LoginRequest;
import com.joshmlwood.demouserservice.contract.RefreshRequest;
import com.joshmlwood.demouserservice.domain.User;
import com.joshmlwood.demouserservice.interfaces.LoginChain;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class RightsAgreementChain implements LoginChain {

    @Override
    public Optional<AuthenticationResult> authenticate(User user, AuthenticationToken tokenHolder, LoginRequest request) {
        if (userRequiresRightsAgreement(user.getRoles()) && !request.getAgreement()) {
            return buildResult();
        }

        return Optional.empty();
    }

    @Override
    public Optional<AuthenticationResult> authenticate(User user, AuthenticationToken tokenHolder, RefreshRequest request) {
        // TODO: 2019-06-23
        return Optional.empty();
    }

    private boolean userRequiresRightsAgreement(List<String> roles) {
        return false;
    }

    private Optional<AuthenticationResult> buildResult() {
        AuthenticationResult result = AuthenticationResult.builder()
                .authenticated(true)
                .requiresRightsAgreement(true)
                .build();

        return Optional.of(result);
    }
}
