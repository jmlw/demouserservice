package com.joshmlwood.demouserservice.config.security;

import com.joshmlwood.demouserservice.exception.EmailAlreadyRegistered;
import com.joshmlwood.demouserservice.exception.InvalidEmailFormat;
import com.joshmlwood.demouserservice.interfaces.LoginChain;
import com.joshmlwood.demouserservice.util.SimpleFactory;
import com.joshmlwood.demouserservice.util.SimpleProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;

//import com.joshmlwood.demouserservice.TwoFactorChain;
//import com.joshmlwood.demouserservice.service.RegistrationService;

@Configuration
public class AuthenticationFlowProviderConfiguration {
    @Bean(name = "loginChainProvider")
    public SimpleProvider<LoginChain> loginChainProvider(
            RightsAgreementChain rightsAgreementChain,
//            TwoFactorChain twoFactorChain,
            AuthenticationChain authenticationChain) {
        SimpleProvider<LoginChain> provider = new SimpleProvider<>(new ArrayList<>());

        provider.register(rightsAgreementChain);
//        provider.register(twoFactorChain);
        provider.register(authenticationChain);

        return provider;
    }

    @Bean
    public SimpleFactory<Class<? extends Exception>, String> userRegistrationExceptionMessages() {
        SimpleFactory<Class<? extends Exception>, String> factory = new SimpleFactory<>("Unknown Error");

        factory.register(InvalidEmailFormat.class, "Provided email is not formatted correctly");
        factory.register(EmailAlreadyRegistered.class, "User with this email already exists");

        return factory;
    }
}
