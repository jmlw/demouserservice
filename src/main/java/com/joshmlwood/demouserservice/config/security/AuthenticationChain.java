package com.joshmlwood.demouserservice.config.security;

import com.joshmlwood.demouserservice.contract.AuthenticationResult;
import com.joshmlwood.demouserservice.contract.AuthenticationToken;
import com.joshmlwood.demouserservice.contract.LoginRequest;
import com.joshmlwood.demouserservice.contract.RefreshRequest;
import com.joshmlwood.demouserservice.domain.User;
import com.joshmlwood.demouserservice.interfaces.LoginChain;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AuthenticationChain implements LoginChain {
    @Override
    public Optional<AuthenticationResult> authenticate(User user, AuthenticationToken token, LoginRequest request) {
        AuthenticationResult result = AuthenticationResult.builder()
                .authenticated(true)
                .fullyAuthenticated(true)
                .authenticationToken(token)
                .build();

        return Optional.of(result);
    }

    @Override
    public Optional<AuthenticationResult> authenticate(User user, AuthenticationToken token, RefreshRequest request) {
        AuthenticationResult result = AuthenticationResult.builder()
                .authenticated(true)
                .fullyAuthenticated(true)
                .authenticationToken(token)
                .build();

        return Optional.of(result);
    }
}
