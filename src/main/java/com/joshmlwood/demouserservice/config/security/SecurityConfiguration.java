package com.joshmlwood.demouserservice.config.security;

import com.auth0.spring.security.api.JwtWebSecurityConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Value("${auth0.issuer}")
    private String auth0Issuer;

    @Value("${auth0.management.clientId}")
    private String auth0Audience;
//
//    @Autowired
//    private Auth0AuthenticationFilter auth0AuthenticationFilter;
//
//    @Autowired
//    private Auth0ImpersonationFilter auth0ImpersonationFilter;
//
//    @Bean(name = "grantedAuthorityServices")
//    public List<GrantedAuthorityProvider> grantedAuthorityServices(
//            LicenceGrantedAuthorityProvider licenceGrantedAuthorityProvider) {
//        return singletonList(licenceGrantedAuthorityProvider);
//    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        JwtWebSecurityConfigurer
                .forRS256(auth0Audience, auth0Issuer)
                .configure(http)
                .authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                .antMatchers(HttpMethod.POST, "/auth/register").permitAll()
                .antMatchers(HttpMethod.POST, "/auth/register/*").permitAll()
                .antMatchers(HttpMethod.POST, "/auth/verify").permitAll()
                .antMatchers(HttpMethod.POST, "/auth/passwordreset").permitAll()
                .antMatchers(HttpMethod.POST, "/auth/login").permitAll()
                .antMatchers(HttpMethod.POST, "/auth/login/*").permitAll()
                // GET /rest/oauth/token is not authenticated
                .antMatchers(HttpMethod.GET, "/auth/token").permitAll()
                // the other REST APIs are authenticated
                .antMatchers("/rest/**").authenticated();
//                .antMatchers("/rest/**").hasRole("VERIFIED_USER");
//                .and()
//                .addFilterBefore(auth0AuthenticationFilter, AnonymousAuthenticationFilter.class)
//                .addFilterBefore(auth0ImpersonationFilter, AnonymousAuthenticationFilter.class);
    }
}
