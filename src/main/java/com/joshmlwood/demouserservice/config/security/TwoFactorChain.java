package com.joshmlwood.demouserservice;

import com.joshmlwood.demouserservice.contract.AuthenticationResult;
import com.joshmlwood.demouserservice.contract.AuthenticationToken;
import com.joshmlwood.demouserservice.contract.LoginRequest;
import com.joshmlwood.demouserservice.contract.RefreshRequest;
import com.joshmlwood.demouserservice.domain.TwoFactorToken;
import com.joshmlwood.demouserservice.domain.TwoFactorTokenRepository;
import com.joshmlwood.demouserservice.domain.User;
import com.joshmlwood.demouserservice.interfaces.TwoFactorEmailService;
import com.joshmlwood.demouserservice.interfaces.LoginChain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class TwoFactorChain implements LoginChain {
    private static final String TWO_FACTOR_AUTHENTICATION_ROLE = "ROLE_TWO_FACTOR_AUTHENTICATION";

    private TwoFactorTokenRepository twoFactorTokenRepository;
    private TwoFactorEmailService twoFactorEmailService;

    @Autowired
    public TwoFactorChain(
            TwoFactorTokenRepository twoFactorTokenRepository,
            TwoFactorEmailService twoFactorEmailService) {
        this.twoFactorTokenRepository = twoFactorTokenRepository;
        this.twoFactorEmailService = twoFactorEmailService;
    }

    private Optional<AuthenticationResult> buildResult() {
        AuthenticationResult result = AuthenticationResult.builder()
                .authenticated(true)
                .requiresSecondFactor(true)
                .build();

        return Optional.of(result);
    }

    @Override
    public Optional<AuthenticationResult> authenticate(User user, AuthenticationToken tokenHolder, LoginRequest request) {
        if (user.isTwoFactorRequired()) {
            TwoFactorToken twoFactorToken = new TwoFactorToken(
                    null,
                    UUID.randomUUID(),
                    user.getEmail(),
                    LocalDateTime.now()
            );
            twoFactorTokenRepository.save(twoFactorToken);

            twoFactorEmailService.sendEmail(user.getEmail());

            return buildResult();
        }

        return Optional.empty();
    }

    @Override
    public Optional<AuthenticationResult> authenticate(User user, AuthenticationToken tokenHolder, RefreshRequest request) {
        return Optional.empty();
    }
}
