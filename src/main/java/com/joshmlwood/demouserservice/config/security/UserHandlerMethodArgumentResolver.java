package com.joshmlwood.demouserservice.config.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.joshmlwood.demouserservice.contract.Authenticated;
import com.joshmlwood.demouserservice.domain.User;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.support.WebArgumentResolver;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import java.io.IOException;
import java.util.Optional;

@Component
@AllArgsConstructor
public class UserHandlerMethodArgumentResolver implements HandlerMethodArgumentResolver {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserHandlerMethodArgumentResolver.class);
    private static final String USER_HEADER = "user";
    private ObjectMapper objectMapper;

    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        // TODO: 2019-06-23 Also check for authenticated annotation which may contain set of roles required
        return methodParameter.getParameterType().equals(User.class);
    }

    @Override
    public Object resolveArgument(MethodParameter methodParameter,
                                  ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest,
                                  WebDataBinderFactory binderFactory) throws Exception {
        if (this.supportsParameter(methodParameter)) {
            User user = convert(webRequest);

            if (isAccountSupportedForMethod(methodParameter, user)) {
                return user;
            }
        }
        return WebArgumentResolver.UNRESOLVED;
    }

    private User convert(WebRequest webRequest) {
        return Optional.ofNullable(webRequest.getHeader(USER_HEADER))
                .map(it -> {
                    try {
                        return objectMapper.readValue(it, User.class);
                    } catch (IOException e) {
                        LOGGER.error(String.format("Failed reading User from header! Nested exception is %s", e.getMessage()));
                        e.printStackTrace();
                        return null;
                    }
                }).orElse(null);
    }

    private boolean isAccountSupportedForMethod(MethodParameter methodParameter, User account) {
        if (accountHasRequiredRoles(methodParameter, account)) {
            return true;
        } else {
            throw new NotPermitted();
        }
    }

    private boolean accountHasRequiredRoles(MethodParameter methodParameter, User user) {
        if (methodParameter.hasParameterAnnotation(Authenticated.class)) {
            Authenticated authenticated = methodParameter.getParameterAnnotation(Authenticated.class);
            for (String role : authenticated.roles()) {
                if (!user.getRoles().contains(role)) {
                    throw new RequiredRoleMissing(role);
                }
            }
        }
        return true;
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    public static class RequiredRoleMissing extends RuntimeException {
        public RequiredRoleMissing(String missingRole) {
            super("Missing Role: " + missingRole);
        }
    }
}
