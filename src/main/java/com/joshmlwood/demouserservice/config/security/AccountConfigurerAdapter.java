package com.joshmlwood.demouserservice.config.security;

import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

@Configuration
@NoArgsConstructor
public class AccountConfigurerAdapter extends WebMvcConfigurerAdapter {
    private UserHandlerMethodArgumentResolver userHandlerMethodArgumentResolver;

    @Bean
    public UserHandlerMethodArgumentResolver activeAccountHandlerMethodArgumentResolver() {
        return userHandlerMethodArgumentResolver;
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(activeAccountHandlerMethodArgumentResolver());

        super.addArgumentResolvers(argumentResolvers);
    }
}
