package com.joshmlwood.demouserservice.config;

import com.auth0.client.auth.AuthAPI;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Auth0ApiConfiguration {

    @Bean
    public AuthAPI authAPI(Auth0ConfigurationProperties properties) {
        String domain = properties.getDomain();
        String clientId = properties.getClientId();
        String clientSecret = properties.getClientSecret();
        return new AuthAPI(domain, clientId, clientSecret);
    }
}
