package com.joshmlwood.demouserservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class DemoUserServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoUserServiceApplication.class, args);
    }

}
