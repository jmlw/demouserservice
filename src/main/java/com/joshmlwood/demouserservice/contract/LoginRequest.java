package com.joshmlwood.demouserservice.contract;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class LoginRequest extends AuthenticationRequest {
    private String password;
    private Boolean rememberMe;
    private Boolean agreement;
}
