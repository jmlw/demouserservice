package com.joshmlwood.demouserservice.contract;

import com.auth0.json.auth.TokenHolder;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor
@AllArgsConstructor
public class Auth0ManagementToken {
    private TokenHolder tokenHolder;

    public TokenHolder getTokenHolder() {
        return tokenHolder;
    }

    public void setTokenHolder(TokenHolder tokenHolder) {
        this.tokenHolder = tokenHolder;
    }
}
