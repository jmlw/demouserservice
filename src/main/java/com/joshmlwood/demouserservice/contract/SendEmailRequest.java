package com.joshmlwood.demouserservice.contract;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
public class SendEmailRequest {
    private List<String> to;
    private String from;
    private String template;
    private String subject;
    private Map<String, Object> templateParams;
}
