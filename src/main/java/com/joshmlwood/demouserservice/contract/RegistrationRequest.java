package com.joshmlwood.demouserservice.contract;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
public class RegistrationRequest {
    @NotBlank
    private String email;

    @NotBlank
    @Length(min = 8)
    private String password;


}
