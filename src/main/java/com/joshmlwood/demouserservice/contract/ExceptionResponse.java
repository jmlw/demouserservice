package com.joshmlwood.demouserservice.contract;

import org.springframework.ui.ModelMap;

public class ExceptionResponse {
    private String message;
    private ModelMap errors;

    public ExceptionResponse(String message) {
        this(message, null);
    }

    public ExceptionResponse(String message, ModelMap errors) {
        this.message = message;
        this.errors = errors;
    }

    public String getMessage() {
        return message;
    }

    public ModelMap getErrors() {
        return errors;
    }
}
