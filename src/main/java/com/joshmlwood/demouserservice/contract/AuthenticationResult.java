package com.joshmlwood.demouserservice.contract;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
public class AuthenticationResult {
    private Boolean authenticated = false;
    private Boolean fullyAuthenticated = false;
    private Boolean requiresPasswordReset = false;
    private Boolean requiresSecondFactor = false;
    private Boolean requiresRightsAgreement = false;
    private AuthenticationToken authenticationToken;
}
