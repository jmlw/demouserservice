package com.joshmlwood.demouserservice.contract;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
public class AuthenticationToken {
    private String accessToken;
    private String refreshToken;
    private long expiresIn;
}
