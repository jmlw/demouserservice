package com.joshmlwood.demouserservice.contract;

import lombok.Getter;

@Getter
public abstract class AuthenticationRequest {
    private String email;
}
