package com.joshmlwood.demouserservice.contract;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class RefreshRequest extends AuthenticationRequest {
    private String refreshToken;
}
