package com.joshmlwood.demouserservice.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class SimpleFactory<K, V> {
    private Map<K, V> keyValueMap = new HashMap<>();
    private V defaultValue;

    public SimpleFactory() {
        this(null);
    }

    public SimpleFactory(V defaultValue) {
        this.defaultValue = defaultValue;
    }

    public V get(K key) {
        V value = keyValueMap.get(key);

        if (Objects.isNull(value)) {
            return Objects.requireNonNull(defaultValue, "Key is not supported, and no default value is set.");
        }

        return value;
    }

    public void register(K key, V value) {
        keyValueMap.put(key, value);
    }
}
