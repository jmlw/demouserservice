package com.joshmlwood.demouserservice.util;

import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

public class OptionalChain<P, E> {
    private Iterator<P> providers;
    private Function<P, Optional<E>> chainFunction;

    private OptionalChain(Iterator<P> providers, Function<P, Optional<E>> chainFunction) {
        this.providers = providers;
        this.chainFunction = chainFunction;
    }

    public static <P, E> Optional<E> chain(Collection<P> providers, Function<P, Optional<E>> chainFunction) {
        Objects.requireNonNull(providers);
        Objects.requireNonNull(chainFunction);

        return new OptionalChain<>(providers.iterator(), chainFunction)
                .findFirst();
    }

    private Optional<E> findFirst() {
        if (providers.hasNext()) {
            Optional<E> optional = chainFunction.apply(providers.next());

            return optional.isPresent() ? optional : findFirst();
        }
        return Optional.empty();
    }
}
