package com.joshmlwood.demouserservice.util;

import java.util.Collection;
import java.util.HashSet;
import java.util.stream.Stream;

public class SimpleProvider<E> {
    private Collection<E> entries;

    public SimpleProvider() {
        this(new HashSet<>());
    }

    public SimpleProvider(Collection<E> entries) {
        this.entries = entries;
    }

    public Collection<E> getEntries() {
        return entries;
    }

    public Stream<E> stream() {
        return entries.stream();
    }

    public void register(E entry) {
        entries.add(entry);
    }
}
