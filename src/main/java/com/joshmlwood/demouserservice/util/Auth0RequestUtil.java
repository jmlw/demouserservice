package com.joshmlwood.demouserservice.util;

import com.auth0.exception.Auth0Exception;
import com.auth0.net.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Auth0RequestUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(Auth0RequestUtil.class);

    public Auth0RequestUtil() {
    }

    public static <T> T executeRequest(Request<T> request) {
        try {
            return request.execute();
        } catch (Auth0Exception var2) {
            LOGGER.error("Could not execute Auth0 request", var2);
            throw new RuntimeException(var2);
        }
    }
}
