package com.joshmlwood.demouserservice.domain;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.UUID;

public interface TwoFactorTokenRepository extends CrudRepository<TwoFactorToken, Long> {
    public Optional<TwoFactorToken> findByToken(UUID token);
}
