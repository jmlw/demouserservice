package com.joshmlwood.demouserservice.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class TwoFactorToken {
    @Id
    @GeneratedValue
    private Long id;
    @Column(unique = true)
    private UUID token;
    @Column(unique = true)
    private String email;
    private LocalDateTime dateCreated;
}
