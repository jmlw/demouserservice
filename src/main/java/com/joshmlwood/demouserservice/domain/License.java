package com.joshmlwood.demouserservice.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class License {
    @Id
    private Long id;
    private String userId;
    private String license;
    private String application;
    private String description;
}
