package com.joshmlwood.demouserservice.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder(toBuilder = true)
@Entity
public class User {
    @Id
    @GeneratedValue
    private Long id;
    private String email;
    private Boolean emailVerified;
    private Boolean passwordResetRequired;
    private Boolean verified;
    //    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
//    @JoinColumn(name = "user_id")
//    private List<ManagedUser> managedUsers;
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "user_id")
    private ManagedUser managedUser;
    @ElementCollection
    private List<String> roles;

    private boolean twoFactorRequired = false;
}
