package com.joshmlwood.demouserservice.domain;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder(toBuilder = true)
@ToString
@Entity
public class ManagedUser {
    @Id
    @GeneratedValue
    private Long id;
    private String provider;
    private String providerId;
}
