package com.joshmlwood.demouserservice.service;

import com.joshmlwood.demouserservice.DemoUserServiceApplication;
import com.joshmlwood.demouserservice.domain.ManagedUser;
import com.joshmlwood.demouserservice.domain.User;
import com.joshmlwood.demouserservice.service.auth0ManagedUser.Auth0ManagementApiService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

@SpringBootTest
@ContextConfiguration(classes = {DemoUserServiceApplication.class})
@ExtendWith(SpringExtension.class)
class Auth0ManagementApiServiceTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(Auth0ManagementApiServiceTest.class);

    @Autowired
    private Auth0ManagementApiService auth0ManagementApiService;

    @BeforeEach
    void setUp() {
    }

    @Test
    void justTryBasicStuff() {
        auth0ManagementApiService.getAllManagedUsers(new Pageable() {
            @Override
            public int getPageNumber() {
                return 0;
            }

            @Override
            public int getPageSize() {
                return 10;
            }

            @Override
            public long getOffset() {
                return 0;
            }

            @Override
            public Sort getSort() {
                return null;
            }

            @Override
            public Pageable next() {
                return null;
            }

            @Override
            public Pageable previousOrFirst() {
                return null;
            }

            @Override
            public Pageable first() {
                return null;
            }

            @Override
            public boolean hasPrevious() {
                return false;
            }
        }).stream().forEach(it -> LOGGER.info(it.toString()));
        User user = User.builder()
                .email("josh.m.l.wood@gmail.com")
                .emailVerified(false)
                .build();
        ManagedUser jw = auth0ManagementApiService.createUser(user, "");
        LOGGER.info(jw.toString());
    }

    @Test
    void findOneByUserId() {
    }

    @Test
    void findOneByEmailIgnoringCase() {
        Optional<ManagedUser> u = auth0ManagementApiService.findOneByEmailIgnoringCase("josh.m.l.wooD@gmail.com");
        u.ifPresent(it -> LOGGER.info(it.getProviderId()));
    }

    @Test
    void createUser() {
    }

    @Test
    void deleteUser() {
        Optional<ManagedUser> u = auth0ManagementApiService.findOneByEmailIgnoringCase("josh.m.l.wooD@gmail.com");
        u.ifPresent(it -> auth0ManagementApiService.deleteUser(it.getProviderId()));
    }

    @Test
    void changePassword() {
    }

    @Test
    void getAllManagedUsers() {
    }

    @Test
    void authenticateUser() {
    }

    @Test
    void refreshToken() {
    }

    @Test
    void enableAccount() {
    }

    @Test
    void disableAccount() {

    }
}
